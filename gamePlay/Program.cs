﻿using System;

namespace gamePlay
{
    public class Game
    {

        int Roll = 0;
        int[] Rolls = new int[21];

        public void roll(int pinsDown)
        {
            Rolls[Roll++] = pinsDown;
        }


        public int Score()
        {
            int score = 0;
            int cursor = 0;

            for (int frame = 0; frame < 10; frame++)
            {
                if(iStrike(cursor))
                {
                    score += 10 + StrikeBonus(cursor);
                    cursor += 1;
                }
                else if (isSpare(cursor))
                {
                    score += 10 + SpareBonus(cursor);
                    cursor += 2;
                }
                else 
                {
                    score += Rolls[cursor] + Rolls[cursor+1];
                    cursor += 2;
                }
            }

            return score;
        }

        public Boolean isSpare(int cursor)
        {
            return Rolls[cursor] + Rolls[cursor + 1] == 10;
        }

        public Boolean iStrike(int cursor)
        {
            return Rolls[cursor] == 10;
        }

        public int StrikeBonus(int cursor)
        {
            return Rolls[cursor + 1] + Rolls[cursor + 2];
        }

        public int SpareBonus(int cursor)
        {
            return Rolls[cursor + 2];
        }

        public static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
    }
}
