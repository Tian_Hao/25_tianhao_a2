﻿using NUnit.Framework;
using System;
using gamePlay;

namespace Testing 
{
    [TestFixture()]
    public class Test
    {

        Game game;

        [SetUp]
        public void SetUpGame()
        {
            game = new Game();
        }

        public void roll(int times, int pinsDown)
        {
            for (int i = 0; i < times;i++ )
            {
                game.roll(pinsDown);
            }
        }


        [Test()]
        public void GutterGame()
        {
            roll(20, 0);
            Assert.That(game.Score(), Is.EqualTo(0));
        }



        [Test()]
        public void OnesGame()
        {
            roll(20, 1);
            Assert.That(game.Score(), Is.EqualTo(20));
        }



        [Test()]
        public void OneSpareGame()
        {
            roll(1,5);
            roll(1, 5);
            roll(18, 0);
            Assert.That(game.Score(), Is.EqualTo(10));
        }

        [Test()]
        public void OneStrikeGame()
        {
            roll(1, 10);
            roll(19, 0);
            Assert.That(game.Score(), Is.EqualTo(10));
        }

        [Test()]
        public void PrefectGame()
        {
            roll(12, 10);
            Assert.That(game.Score(), Is.EqualTo(300));
        }

        [Test()]
        public void TwosGame()
        {
            roll(20, 2);
            Assert.That(game.Score(), Is.EqualTo(40));
        }

        [Test()]
        public void ThreesGame()
        {
            roll(20, 3);
            Assert.That(game.Score(), Is.EqualTo(60));
        }

        [Test()]
        public void AllSpareGame()
        {
            roll(21, 5);
            Assert.That(game.Score(), Is.EqualTo(150));
        }
    }
}
